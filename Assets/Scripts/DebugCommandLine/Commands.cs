using System;
using System.Collections.Generic;
using UnityEngine;
using HunT.CommandLine;

public static class Commands {
    private static Dictionary<(int, string), Command> commands = new Dictionary<(int, string), Command>();

    public static void AddCommand(string command, int expectedArguments, ExecuteCommandHandler executeCommand) {
        try {
            commands.Add((expectedArguments, command.ToUpper()), new Command(expectedArguments, executeCommand));
        }
        catch (ArgumentException) {
            Debug.LogWarning($"Command \"{command}\" already exists!");
        }
    }

    public static void ExecuteCommand(string name, params string[] arguments) {
        try {
            commands[(arguments.Length, name.ToUpper())].Execute(arguments);
        }
        catch (KeyNotFoundException) {
            Debug.LogWarning($"Command \"{name}\" with {arguments.Length} number of arguments not found!");
        }
        catch (Exception e) {
            Debug.LogWarning($"Error on execute command: {e.Message}");
        }
    }
}
