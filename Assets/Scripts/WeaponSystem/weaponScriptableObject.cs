using UnityEngine;

[CreateAssetMenu(fileName = "New Weapon", menuName = "Weapon")]
public class weaponScriptableObject : ScriptableObject
{
    public new string name;
    public string description;
    public string weaponType;
    public string ammunitionType;

    public GameObject model;

}
